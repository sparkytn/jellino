@extends('layouts.app')

@section('content')
    @if( session()->get('first_time') )
        <div class="loader show how-to-modal">
            <div class="how-to-wrapper d-flex  h-100 justify-content-center align-items-center text-center p-5 ">
                <img src="{{ asset('images/how-to-card.png') }}" class="img-fluid" alt="How To">
            </div>
        </div>
    @endif

    <div class="logo">
        <img src="{{ asset('images/logo-game.png') }}" class="img-fluid" alt="">
    </div>
    <div class="game-board h-100">



        <div class="timer-wrapper">
            <div class="timer"></div>
        </div>

        <div class="moves d-none"></div>

        <div class="score-wrap">
            <div class="stars"></div>
            <span class="score"></span>
        </div>
        <div class="row justify-content-center align-items-center h-100"  id="game-wrapper">

            <ul class="card-wrap d-flex justify-content-center align-content-center level-1" id="card-wrap1">
                @for($i = 1; $i<=3; $i++)
                    <li class="card-game card-3" type="{{ config('app.cards')['card'.$i][2] }}"><img src="{{ asset("images/cards/".config('app.cards')['card'.$i][0].".png") }}" class="img-fluid" alt=""></li>
                    <li class="card-game card-3" type="{{ config('app.cards')['card'.$i][2] }}"><img src="{{ asset("images/cards/".config('app.cards')['card'.$i][1].".png") }}" class="img-fluid" alt=""></li>
                @endfor
            </ul>
            <ul class="card-wrap d-none justify-content-center align-content-center level-2" id="card-wrap2">
                @for($i = 1; $i<=4; $i++)
                    <li class="card-game card-4" type="{{ config('app.cards')['card'.$i][2] }}"><img src="{{ asset("images/cards/".config('app.cards')['card'.$i][0].".png") }}" class="img-fluid" alt=""></li>
                    <li class="card-game card-4" type="{{ config('app.cards')['card'.$i][2] }}"><img src="{{ asset("images/cards/".config('app.cards')['card'.$i][1].".png") }}" class="img-fluid" alt=""></li>
                @endfor
            </ul>
            <ul class="card-wrap d-none justify-content-center align-content-center level-3" id="card-wrap3">
                @for($i = 1; $i<=5; $i++)
                    <li class="card-game card-5" type="{{ config('app.cards')['card'.$i][2] }}"><img src="{{ asset("images/cards/".config('app.cards')['card'.$i][0].".png") }}" class="img-fluid" alt=""></li>
                    <li class="card-game card-5" type="{{ config('app.cards')['card'.$i][2] }}"><img src="{{ asset("images/cards/".config('app.cards')['card'.$i][1].".png") }}" class="img-fluid" alt=""></li>
                @endfor
            </ul>

        </div>
    </div>
    <div class="win-board h-100 d-none">
        <div class="score-wrap final">
            <div class="stars"></div>
            <span class="score">0</span>
        </div>
        <a href="/accueil" class="btn-img btn-reply">
            <img src="{{ asset('images/btn-reply.png') }}" class="img-fluid" alt="">
        </a>
    </div>
@endsection
