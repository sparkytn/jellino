@extends('layouts.app')

@section('class', 'login-page')

@section('content')
    <div class="container-fluid h-100" id="signup">
        <div class="row justify-content-center align-items-center h-100 page-1">
            <a href="#" id="fb-btn" class="btn-img btn-login-fb disabled" js-fb-connect>
                <img src="{{ asset('images/btn-play.png') }}"   alt="">
            </a>
        </div>
        <div class="row justify-content-center align-items-center h-100 page-2 d-none">
            <div class="col-md-2 col-lg-2 text-center pt-5 d-md-flex d-none">
                <img src="{{ asset('images/smurfs.png') }}" class="img-fluid" alt="">
            </div>


            <div class="col-md-6 col-lg-4">
                <div class="text-center">
                    <img src="{{ asset('images/logo-game.png') }}" width="300px" class="register-logo" alt="">
                </div>

                <div class="card  border-0  bg-transparent">
                    <div class="card-body text">

                        <h3 class="text-center mb-4 text-primary">Inscription</h3>

                        <form method="POST" id="signup-form" action="{{ route('inscription') }}">
                            @csrf  
                            <input type="hidden" name="fb_id" id="fb_id">
                            <input type="hidden" name="email" id="email">
                            <input type="hidden" name="password" id="password">
                            <div class="form-group">
                                <label for="email" class="t">Votre nom et prénom</label>

                                <div class="">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name"  value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="">Téléphone</label>

                                <div class="">
                                    <input id="phone" type="tel" class="form-control @error('phone') is-invalid @enderror" name="phone" maxlength="8" minlength="8" required>

                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group mb-0 mt-5 text-center">
                                    <button type="submit" class="btn btn-img btn-play">
                                        <img src="{{ @asset('images/btn-play.png') }}" alt="">
                                    </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-lg-2 text-center pt-5 d-md-flex d-none">
                <img src="{{ asset('images/bottle.png') }}" class="img-fluid" alt="">
            </div>
        </div>
    </div>
@endsection