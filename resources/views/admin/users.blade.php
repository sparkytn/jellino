@extends('admin.layouts.app')

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Utilisateur</h1>
{{--        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>--}}
    </div>

    <div class="row justify-content-center">

        <div class="card w-100">
                @foreach($players as $player)
                    <li class="list-group-item p-4">
                        <div class="row align-items-center">
                            <div class="col">
                                <div class="row align-items-center">
                                    <div class="col-md-7 d-flex">
                                <span>
                                    <img src="{{ Avatar::create($player->name)->toBase64() }}" width="30px" class=" border rounded-pill border-white float-left" />
                                </span>
                                        <div class="d-inline-block float-left col">
                                            <h5 class="d-block m-0">{{ $player->name }}</h5>
                                            <i class="fas fa-envelope"></i> <small class="">{{ $player->email }} |  <i class="fas fa-calendar"></i> {{ $player->created_at  }}</small>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </li>
                @endforeach
        </div>

    </div>


@endsection
