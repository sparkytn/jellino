@extends('admin.layouts.app')

@section('content')

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <a href="{{ route('admin.export') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>

    <div class="row justify-content-center">

        <!-- Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Users</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{$usersCount}}</div>
                                </div>
                                <div class="col">
                                   Utilisateurs
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-users fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Nombre</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{$gameCount}}</div>
                                </div>
                                <div class="col">
                                    Jeux
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-gamepad fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>


    <div class="row justify-content-center">

        <ul class="list-group w-100">

            @foreach($users as $user)
                <li class="list-group-item d-flex align-items-center">
                    <div class="mr-3">
                        <img src="http://graph.facebook.com/{{ $user->fb_id }}/picture?type=square" class="rounded-circle" alt="">
                    </div>
                    <div class="mr-3">
                        <h5 class="text-primary mb-1">{{ $user->name }} <small>ID : {{ $user->fb_id }}</small></h5>
                        {{ $user->email }} - {{ $user->phone }}  - Total games :{{ $user->games_count }} - <small class="text-muted"> <i class="fas fa-clock"></i> {{ $user->timeSpent() }}</small>
                    </div>
                    <div class="ml-auto text-primary text-right">
                        <h5 class="mb-0">
                            <i class="fas fa-star"></i> {{ $user->maxScore() }}

                        </h5>

                    </div>



                </li>
            @endforeach
        </ul>


    </div>


@endsection
