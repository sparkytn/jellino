@extends('layouts.app')

@section('content')

    <div class="col-lg-12 mb-4 align-items-stretch">
        <div class="card border-0 shadow h-100 overflow-hidden">
            <div class="row no-gutters h-100 align-items-stretch">
                <div class="col-md-3 col-lg-3 align-self-center">
                    <img src="{{asset('images/games/cover-'.$game->id.'.png')}}" class="card-img">
                </div>
                <div class="col-md-6 col-lg-7">
                    <div class="card-body">
                        <h4 class="card-title text-primary">{{ $game->name }}</h4>
                        <p class="card-text">{!! $game->description !!}</p>
                    </div>
                </div>

                <div class="col-md-3 col-lg-2 text-center align-self-center">
                    <a href="{{ route('game') }}" class="btn btn-lg btn-primary mb-3 btn-lg">Jouer</a>
                </div>
            </div>
        </div>
    </div>


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="mt-5">
                <h3 class="text-primary my-4">
                    <i class="fas fa-trophy text-white bg-warning d-inline-block p-3 rounded-pill mr-2"></i>
                    Classement des Joueurs dans {{ $game->name }}
                </h3>
                @include('partials.players-list-all'  , ['players'=> $photos])
        </div>


        </div>
    </div>
</div>
@endsection
