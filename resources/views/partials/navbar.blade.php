<nav class="navbar navbar-light bg-white shadow-sm">
    <div class="container-fluid">
        <button class="navbar-toggler mr-2 bg-primary" id="menu-toggle" type="button">
            <span class="navbar-toggler-icon"></span>
        </button>


        <div class="ml-auto">
            <!-- Authentication Links -->
            @guest
                @if(\Request::route()->getName() === 'register')
                    <div class="d-inline-block">
                        <a class="nav-link" href="{{ route('login') }}">Connexion</a>
                    </div>
                @endif
                @if (Route::has('register'))
                    <div class="d-inline-block">
                        <a class="nav-link" href="{{ route('register') }}">Inscription</a>
                    </div>
                @endif
            @else
                <div class="d-inline-block">

                        <a class="nav-link d-flex align-items-center" href="/">
                            <div class="d-inline-block mr-2">
                                <img src="{{ Avatar::create(Auth::user()->name)->toBase64() }}" width="30px" />
                            </div>
                            <div class="d-inline-block" style="line-height: 18px;">
                                <small class="text-dark d-block">Bienvenue</small>{{ Auth::user()->name }}
                            </div>

                        </a>
                </div>
            @endguest
        </div>

    </div>
</nav>
