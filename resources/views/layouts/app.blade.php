<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="favicon.png">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />

    <!-- Scripts -->
    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ request()->url() }}';
        APP_ENV     = '{{ app()->environment() }}';
        @guest
			window.loggedIn = false;
        @endguest
        @auth
            window.userName = '{{ Auth::user()->name }}';
            window.loggedIn = true;
        @endauth
    </script>


    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nerko+One&display=swap" rel="stylesheet">


    <!-- Styles -->
    <link href="{{ asset('css/app.css?e=e9') }}" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-3HS3RDZKCV"></script>
    <script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-3HS3RDZKCV');
    </script>


</head>
<body class="@yield('class')">
<script>
	window.fbAsyncInit = ()=> {
		FB.init({
			appId: '1417883621897321', // App ID
			status: true, // check login status
			cookie: true, // enable cookies to allow the server to access the session
			xfbml: true, // parse XFBML
			version : 'v2.7'
		});
		FB.AppEvents.logPageView();

		let btn = document.getElementById("fb-btn");
		btn.classList.remove("disabled");

		FB.getLoginStatus(function(response) {
			window.statusChangeCallback(response);
		});

	}
</script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js"></script>

<!--  Loader -->
<div class="loader">
    <div class="spinner">
        <small>   Veuillez Patienter </small>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>

    <div id="app" class="d-flex">
        <main id="content-wrapper" class="d-flex flex-column">

            <div class="p-3 p-md-5 body-content">

                <div class="btn-settings">
                    <a href="#" class="btn-img btn-son active">
                        <img src="{{ asset('images/btn-music.png') }}" alt="">
                    </a>
                </div>

                @yield('content')

            </div>
            <footer class="mt-auto p-2">
                <div class="logo-papillon">
                    <img src="{{ asset('images/logo.png') }}" alt="">
                </div>
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <a href="https://www.facebook.com/BonbonsPapillon" class="mx-2" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            <a href="https://www.instagram.com/bonbons_papillon/" class="mx-2" target="_blank"><i class="fab fa-instagram"></i></a>
                            <div class="d-inline-block float-right">
                                <a href="{{ asset('reglement.pdf') }}" target="_blank" class="mx-1">Règlement</a>
                                <span class="mx-1">
                                © 2021 Papillon
                                </span>
                            </div>
                        </div>
                        <div class="col-6 text-muted text-right">

                        </div>
                    </div>

                </div>
            </footer>
        </main>
    </div>

    <script src="{{ asset('js/app.js?s=e9') }}" defer></script>
    <!-- Load the JS SDK asynchronously -->

</body>
</html>
