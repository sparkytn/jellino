require('./bootstrap');
const Swal = require('sweetalert2');

$(function () {

    var ambientMusic = new Audio();
    ambientMusic.src = '../sfx/music-jellino.mp3';
    ambientMusic.volume= 0.2;
    ambientMusic.loop = true;
    ambientMusic.play();
    
    let sfxFlip = new Audio('../sfx/flip.mp3'),
        sfxWrong = new Audio('../sfx/wrong.mp3'),
        sfxWin = new Audio('../sfx/winner.mp3'),
        sfxSuccess = new Audio('../sfx/success.mp3'),
        sfxNextlevel = new Audio('../sfx/nextLevel.mp3');

    $('.btn-son').click(function(){
        if( $(this).hasClass('active') ) {
            ambientMusic.pause();
        }else{
            ambientMusic.play();
        }
        $(this).toggleClass('active');
    });


    window.addEventListener("blur", onWindowBlur, false);

    function onWindowBlur (){
        ambientMusic.pause();
    }


    $('.loader').addClass('hide');
    function showLoader( progress = false) {
        if(!progress) {
            $('.loader').removeClass('hide');
            $('body').css('overflow', 'hidden');
        }else{
            $('.progress-wrapper').removeClass('hide');
            $('body').css('overflow', 'hidden');
        }
    }
    function hideLoader( progress = false) {
        if(!progress) {
            $('.loader').addClass('hide');
            $('body').css('overflow', 'visible');
        }else{
            $('.progress-wrapper').addClass('hide');
            $('body').css('overflow', 'visible');
        }
    }


    // hide how to when click
    $('.how-to-modal img').on('click',function () {
        $('.how-to-modal').fadeOut(300,function(){
            $('.how-to-modal').removeClass('show');
        });
    })

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        error: function(jqXHR) {
            $('body').removeClass('disabled-for-ajax');
            if ( /<html>/i.test(jqXHR.responseText))
                alert('Une erreur serveur s\'est produite, veuillez réessayer ultérieurement.');
            else
                alert(jqXHR.responseText);
        }
    });

    let userLoggedIn = null;
    window.statusChangeCallback = function(response) {  // Called with the results from FB.getLoginStatus().
        if (response.status === 'connected') {   // Logged into your webpage and Facebook.
            userLoggedIn = response.authResponse.userID;
            console.log(userLoggedIn);
        }
    }


    $('[js-fb-connect]').click(function() {

        if($(this).hasClass('disabled') ){
            return false;
        }
        showLoader();

        $(this).addClass('disabled');

        if(window.loggedIn){
            return document.location = '/accueil';
        }

        if(userLoggedIn != null){
            $.ajax({
                url: '/check',
                method: 'post',
                data: { uid : userLoggedIn}
            }).done(function(response) {
                if( ! response.registred ){
                    return getUserFBInfo();
                }
                location.reload();
            });

            return ;

        }

        FB.login(function(response) {

            if (response && response.authResponse) {

                $.ajax({
                    url: '/check',
                    method: 'post',
                    data: { uid : response.authResponse.userID}
                }).done(function(response) {
                    if( ! response.registred ){
                        return getUserFBInfo();
                    }
                    location.reload();
                });

            } else {
                hideLoader();
                Swal.fire({
                    title: 'Non connecter',
                    confirmButtonText: 'Fermer',
                    allowOutsideClick: false,
                    html: 'Vous devez vous connectez à Facebook.',
                    icon: 'warning', // info, error, warning
                    width: 600,

                });
                $('[js-fb-connect]').removeClass('disabled');
            }
        },{scope: 'public_profile,email'});

    })

    function getUserFBInfo() {
        FB.api('/me','GET', {fields: 'id,name,email'}, function(response) {
            hideLoader();
            $('.page-1').addClass('d-none');
            $('.page-2').removeClass('d-none');
            $('#app').addClass('register')
            userName = response.name;
            $('input#name').val(response.name);
            $('input#email').val(response.email);
            $('input#fb_id').val(response.id);
            $('input#password').val(response.id);
        });
    }



    $('#signup-form').on('submit',function (e) {
        e.preventDefault();

        if($(this).hasClass('disabled')){
            return;
        }
        showLoader();
        $(this).addClass('disabled');

        $.ajax({
            url: '/register',
            method: 'post',
            data: $(this).serialize()
        }).done(function(response) {
            $('#signup-form').removeClass('disabled');


            if( response.success ){
                setTimeout(function () {
                    return  document.location = '/accueil';
                },1000)

            }else{
                hideLoader();
                return Swal.fire({
                    title: 'Oups !',
                    html: response.message,
                    icon: 'error', // info, error, warning
                    confirmButtonText: 'Fermer',
                    width: 600,
                    buttonsStyling: false,
                    customClass: {
                        confirmButton: 'btn btn-primary mb-3',
                    },
                })
            }

        });

    })



// declaring move variable
    let userName = '';
    let moves = 0;
    let counter = document.querySelector(".moves");

// declare variables for star icons
    const stars = document.querySelectorAll(".fa-star");

// declaring variable of matchedCards
    let matchedCard = document.getElementsByClassName("match");

// stars list
    let starsList = document.querySelectorAll(".stars li");

// close icon in modal
    let closeicon = document.querySelector(".close");

// declare modal
    let modal = document.getElementById("popup1")

// array for opened cards
    var openedCards = [];

    let  gameStarted = false;

    let ScoreRange = [0,50,80,100];
    let userScore = penalty = 0;


    let currentLevel = 1;
    let deck = document.getElementById("card-wrap"+currentLevel);
    let card = document.querySelectorAll("#card-wrap"+currentLevel +".card-game");
    let cards = [...card];

    let second = 0, minute = 0; hour = 0;
    let timer = document.querySelector(".timer");
    let interval;
    let maxTime = 50;



// @description shuffles cards
// @param {array}
// @returns shuffledarray
    function shuffle(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        while (currentIndex !== 0) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    };


// @description shuffles cards when page is refreshed / loads
//document.body.onload = startGame();

    let ui = null, gi = null;
// @description function to start a new play
    function startGame(){

        // empty the openCards array
        openedCards = [];

        // shuffle deck
        cards = shuffle(cards);
        // remove all exisiting classes from each card
        for (var i = 0; i < cards.length; i++){
            deck.innerHTML = "";
            [].forEach.call(cards, function(item) {
                deck.appendChild(item);
            });
            cards[i].classList.remove("show", "open", "match", "disabled");
        }
        // reset moves
        moves = 0;
        counter.innerHTML = moves;
        // reset rating
        for (var i= 0; i < stars.length; i++){
            stars[i].style.color = "#FFD700";
            stars[i].style.visibility = "visible";
        }
        //reset timer
        if(currentLevel == 1){
            second = maxTime;
            var timer = document.querySelector(".timer");
            timer.innerHTML =  maxTime+"<small>Secs</small>";
            clearInterval(interval);
        }

    }


// @description toggles open and show class to display cards
    var displayCard = function (){
        this.classList.toggle("open");
        this.classList.toggle("show");
        this.classList.toggle("disabled");
    };


// @description add opened cards to OpenedCards list and check if cards are match or not
    function cardOpen() {
        openedCards.push(this);

        sfxFlip.play();

        var len = openedCards.length;
        if(len === 2){
            moveCounter();
            if(openedCards[0].type === openedCards[1].type){
                matched();
                userScore += ScoreRange[currentLevel];

            } else {
                unmatched();
                //userScore -= 5;
            }
            $('.score-wrap .score').html(userScore)
        }
        if(!gameStarted){
            startTimer();
            gameStarted  = true;
            $.ajax({
                url: '/start-game',
                method: 'post',
                data: {'start_date': new Date().toISOString().slice(0, 19).replace('T', ' ')}
            }).done(function(response) {

                if( response.success ){
                    pi = response.pi;
                    gi = response.gi;
                }

            });
        }


    };


// @description when cards match
    function matched(){
        openedCards[0].classList.add("match", "disabled");
        openedCards[1].classList.add("match", "disabled");
        openedCards[0].classList.remove("show", "open", "no-event");
        openedCards[1].classList.remove("show", "open", "no-event");
        openedCards = [];
        sfxSuccess.play();
    }


// description when cards don't match
    function unmatched(){
        openedCards[0].classList.add("unmatched");
        openedCards[1].classList.add("unmatched");
        disable();
        setTimeout(function(){
            openedCards[0].classList.remove("show", "open", "no-event","unmatched");
            openedCards[1].classList.remove("show", "open", "no-event","unmatched");
            enable();
            openedCards = [];
        },1100);
        sfxWrong.play();
    }


// @description disable cards temporarily
    function disable(){
        Array.prototype.filter.call(cards, function(card){
            card.classList.add('disabled');
        });
    }


// @description enable cards and disable matched cards
    function enable(){
        Array.prototype.filter.call(cards, function(card){
            card.classList.remove('disabled');
            for(var i = 0; i < matchedCard.length; i++){
                matchedCard[i].classList.add("disabled");
            }
        });
    }


// @description count player's moves
    function moveCounter(){
        moves++;
        counter.innerHTML = moves;
        //start timer on first click
        // if(moves == 1){
        //     second = 0;
        //     minute = 0;
        //     hour = 0
        // }
        // setting rates based on moves
        if (moves > 8 && moves < 12){
            penalty = 5;
        }
        else if (moves > 13){
            penalty =10;
        }
    }


// @description game timer

    function startTimer(){
        interval = setInterval(function(){
            timer.innerHTML = second+"<small>Secs</small>";
            if(second == 0){
                sfxWrong.play();
                clearInterval(interval);
                return Swal.fire({
                    title: 'Temps écoulé!',
                    confirmButtonText: 'Fermer',
                    allowOutsideClick: false,
                    html: 'Vous pouvez rejouer pour améliorer votre score.' ,
                    icon: 'warning', // info, error, warning
                    width: 600,
                    onClose: () => {
                       winScreen(false);
                    }
                });

            }
            second--;
        },1000);
    }


// @description congratulations when all cards match, show modal and moves, time and rating
    function congratulations(){
        if (matchedCard.length == cards.length){


            finalTime = timer.innerHTML;

            if(currentLevel === 3){
                clearInterval(interval);
                sfxWin.play();
                return winScreen();
            }

            sfxNextlevel.play();

            return Swal.fire({
                title: 'Bravo ' + window.userName + '!',
                // showConfirmButton: false,
                confirmButtonText: 'Suivant',
                allowOutsideClick: false,
                html: 'Vous allez passer au niveau '+ (currentLevel + 1) ,
                icon: 'success', // info, error, warning
                width: 600,
                onClose: () => {
                    nextlevel()
                }
            });
        }
    }

// @desciption for user to play Again
    function playGame(level){

        // cards array holds all cards
        card = document.querySelectorAll("#card-wrap"+level +" .card-game");
        cards = [...card];
        deck = document.getElementById("card-wrap"+level);

        startGame();

        // loop to add event listeners to each card
        for (var i = 0; i < cards.length; i++){
            card = cards[i];
            card.addEventListener("click", displayCard);
            card.addEventListener("click", cardOpen);
            card.addEventListener("click",congratulations);
        }
    }

    function nextlevel() {
        $('.match').removeClass('match');
        $('#card-wrap' + currentLevel ).removeClass('d-flex').addClass('d-none')
        currentLevel ++
        $('#card-wrap' + currentLevel ).addClass('d-flex').removeClass('d-none')

        playGame(currentLevel);
    }

    function winScreen( winner = true) {
        showLoader();


        $.ajax({
            url: '/end-game',
            method: 'post',
            data: {
                'end_date': new Date().toISOString().slice(0, 19).replace('T', ' '),
                'time_spent' : second,
                'score' : userScore,
                'level' : currentLevel
            }
        }).done(function() {
            hideLoader();

            if( ! winner ){
                return;
            }

            Swal.fire({
                title: 'Bravo ' + window.userName + '!',
                confirmButtonText: 'Fermer',
                allowOutsideClick: false,
                html: 'Le jeu est terminé, votre score est de : ' + userScore,
                icon: 'success', // info, error, warning
                width: 600,
            });

        });

        $('.game-board').addClass('d-none');
        $('.win-board').removeClass('d-none');
        $('body').addClass('win-page')
    }


    if( $('#game-wrapper').length ){
        playGame(currentLevel);
    }


});



