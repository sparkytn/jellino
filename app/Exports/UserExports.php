<?php
namespace App\Exports;
use App\Models\User;
use App\Models\Game;
use Maatwebsite\Excel\Concerns\FromCollection;

class UserExports implements FromCollection
{
	public function headings():array{
		return[
				'ID',
				'Nom et Prénom',
				'FB ID',
				'Email',
				'Phone',
				'Email',
				'Total Games',
				'Max Score',
		];
	}


    public function collection()
    {
        return User::select(\DB::raw('users.id,users.fb_id,users.name,users.email,users.phone,count(games.id) as totalGames, max(games.score) as maxScore', ))
            ->leftJoin('games', 'games.user_id', '=', 'users.id')
            ->groupBy('users.id')
            ->orderBy('maxScore', 'desc')
            ->get();
    }
}
