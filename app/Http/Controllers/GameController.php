<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UserExports;

use App\Models\Game;

class GameController extends Controller
{

    public $questionArray;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    /**
     * Show raking page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ranking()
    {
        $photos = Game::getActiveGameUsersScore();

        return view('ranking')->with([
            'photos' => $photos
        ]);
    }





    /**
     * Export to excel
     */
    public function startGame(Request $request)
    {
    	session()->forget('first_time');

        $game = new Game();
		$game->user_id = auth()->user()->id;
        $game->level = 1;
        $game->score = 0;
        $game->start_date = $request->start_date;
        $game->time_spent = 0;
        $game->ip = $request->ip();

        if(!$game->save()){
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }

        session([ 'game_id' => $game->id ]);
        return response()->json([
            'success' => true,
            'ui' => auth()->user()->id,
            'gi' => $game->id,
        ]);
    }


    public function endGame(Request $request)
    {
        if(!session('game_id')){
			return response()->json([
					'success' => false,
					'message' => 'Something wrong happened !',
			]);
        }

        $game = Game::find(intval(session('game_id')));
        $game->end_date = $request->end_date;
		$game->time_spent = $request->time_spent;
        $game->score = $request->score;
        $game->level = $request->level;

        if(!$game->save()){
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }

        session()->forget('play_id');

        return response()->json([
            'success' => true,
        ]);


    }

}
