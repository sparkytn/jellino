<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('guest:web')->except('logout');
    }

    // Logout FN

    public function logout(Request $request)
    {
        $route = '/';
//        if( Auth::guard('admin')->check() ){
//            $route= 'admin/login';
//        }

        $this->guard()->logout();
        $request->session()->forget('user_id');
        $request->session()->invalidate();
        return redirect($route);
    }

    // Admin Login

    public function showAdminLoginForm()
    {
        return view('admin.auth.login');
    }

    public function adminLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:4'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            return redirect()->intended('/admin');
        }
        return back()->withInput($request->only('email', 'remember'));
    }


    // Users
    public function authenticated(Request $request , $user)
    {
        if( auth() ) {

            Auth::logoutOtherDevices($request->password);

            $request->session()->put('user_id',$user->id );

            if( !$request->is('admin/*/**','admin')) {
                return redirect()->route('home');
            }

            return redirect()->route('admin.dashboard');
        }

        return redirect('/');

    }
}
