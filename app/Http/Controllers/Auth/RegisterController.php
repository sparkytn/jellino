<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

	function checkRegistred(Request $request)
	{
		$user = User::where('fb_id', $request->uid)->first();

		if($user){
			Auth::login($user);
			return response()->json([
					'success' => true,
					'user' => $user,
					'registred' => true
			]);

		}



		return response()->json([
				'success' => true,
				'user'=>$user,
				'registred' => false
		]);
	}



	public function register(Request $request)
	{
		$validator =  Validator::make($request->all(), [
				'name' => ['required', 'string', 'max:190'],
				'fb_id' => ['required', 'string', 'max:190'],
				'phone' => ['required', 'string', 'max:8', 'unique:users','regex:/^[0-9]{8}$/'],
		]);

		$validator->setAttributeNames([
				'name'                     => 'Nom et prénom',
				'phone'                         => 'Téléphone',
				'fb_id'                      => 'Connectez-vous à Facebook',
		]);

		if( $validator->fails() ){
			return response()->json([
					'success' => false,
					'message'=> implode('<br>', $validator->errors()->all()),
			]);
		}

		$email = $request->email;

		if($request->email == ''){
			$email = $request->fb_id .'@fb.com';
		}

		$user = User::create([
				'name' => $request->name,
				'fb_id' => $request->fb_id,
				'email' => $email,
				'phone' => $request->phone,
				'password' => bcrypt($request->password),
		]);

		Auth::login($user);
		sleep(1);
		session([ 'first_time' => true ]);

		return response()->json([
				'success' => true,
		]);
	}



    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:4'],
            'fb_id' => ['required', 'string', 'min:4'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        return User::create([
            'name' => $data['name'],
            'zitouna_id' => $data['zitouna_id'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }


//    public function showPlayerRegisterForm()
//    {
//        return view('auth.register', ['url' => 'player']);
//    }
}
