<?php

namespace App\Http\Controllers\Admin;

use App\Exports\UserExports;
use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        $usersCount = User::all()->count();
        $gameCount = Game::all()->count();

        $users = User::with('games')->withCount('games')->get();
        return view('admin.home')->with([
			'users'					=>$users,
            'usersCount'            =>$usersCount,
            'gameCount'            	=>$gameCount
        ]);
    }


	/**
	 * Export to excel
	 */
	public function exportExcel()
	{
		return Excel::download(new UserExports, 'jellinowin-user-list.xlsx');
	}
}
