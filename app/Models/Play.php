<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Play extends Model
{

    protected $guard = 'web';

    protected $fillable = [
        'user_id', 'game_id', 'date', 'winner', 'score'
    ];

    public function users(){
        return $this->belongsTo('App\Models\User','user_id');
    }

    public static function getFirstTen()
    {
        $firstTen = collect(Play::with('users')
            ->where('game_id', Game::getActiveGame()->id)
            ->orderBy('plays.score')
            ->get())->unique('user_id')->take(11);

        return $firstTen->all();
    }

    public static function getActiveGameUsersScore()
    {
        $users = collect(Play::with('users')
            ->where('game_id', Game::getActiveGame()->id)
            ->where('winner', 1)
            ->get());
        return $users->unique('user_id')->all();
    }
}
