<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    //

    protected $guard = 'web';

    protected $fillable = [
        'user_id','photo_id', 'ip'
    ];


    public function users(){
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function photos(){
        return $this->belongsTo('App\Models\Photo','photo_id');
    }
}
