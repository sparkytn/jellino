<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    protected $guard = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'fb_id', 'avatar', 'phone', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


	public static function getUsers(){
		$records = DB::table('users')->select('id','name','fb_id','email','phone')->maxScore()->timeSpent()->get()->toArray();
		return $records;
	}


    public function games(){
        return $this->hasMany('App\Models\Game');
    }

    public function maxScore(){
    	return Game::where('user_id',$this->id)->max('score');
	}
	public function timeSpent(){
		return Game::where('user_id',$this->id)->max('time_spent');
	}
}
