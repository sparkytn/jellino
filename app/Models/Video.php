<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //

    protected $guard = 'web';

    protected $fillable = [
        'path', 'user_id', 'description', 'status'
    ];

    public function users(){
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function votes(){
        return $this->hasMany('App\Models\Vote');
    }

    public function getVoteCount(){
        return Vote::where('photo_id',$this->id)->count();
    }

    public function alreadyVoted($user_id){
        return Vote::where('user_id',$user_id)->where('photo_id',$this->id)->count() == 0;
    }
}
