<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@showLoginPage');
Route::post('/check', 'Auth\RegisterController@checkRegistred');
Route::post('/register', 'Auth\RegisterController@register')->name('inscription');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/privacy-policy', 'HomeController@policy');

Route::get('/accueil', 'HomeController@index')->name('home');
Route::get('/classement', 'GameController@ranking')->name('ranking');

Route::post('/start-game', 'GameController@startGame');
Route::post('/end-game', 'GameController@endGame');




Route::get('admin/login', 'Auth\LoginController@showAdminLoginForm')->name('admin.login');
Route::post('admin/login', 'Auth\LoginController@adminLogin');
Route::get('admin/logout', 'Auth\LoginController@logout')->name('logout2');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/', 'AdminController@index')->name('dashboard');
    Route::get('/users', 'AdminController@users')->name('users');
    Route::post('/moderation', 'AdminController@moderation')->name('moderation');
	Route::get('/exportExcel','AdminController@exportExcel')->name('export');
});
